# MovieApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.9.

Install vendor dependencis by running command `npm install`.

Please start compiler with `npm start` as it uses a proxy config.

I used json-server for backend api. 

 json-server needs to be installed globally. More info here [json-server](https://github.com/typicode/json-server)
 
 cd into project `/json-server` folder and run command: `json-server db.json`;
 
The server runs on port `3004`, it can be changed from config file that is in the same folder.
 
I created 2 projects, the other one implements Redux [Redux MovieApp2](https://bitbucket.org/RazvanDinu/movieapp2)

I did this because i was afraid i would not have time to finish and i'll end up with a broken app.
 
I did not have time to write tests. I also learned redux and at first it seemed simple, i mean the concept, the implementation was a bit tricky.
 
 That's it :) 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
