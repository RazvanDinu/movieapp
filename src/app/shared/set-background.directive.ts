import {Directive, HostBinding, Input, OnInit} from '@angular/core';
import {DomSanitizer, SafeStyle} from '@angular/platform-browser';

@Directive({
  selector: '[appSetBackground]'
})
export class SetBackgroundDirective implements OnInit {

  @Input('appSetBackground') backgroundImgName: string;

  @HostBinding('style.background-image') background: SafeStyle | string;

  constructor(protected sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.background = this.sanitizer.bypassSecurityTrustStyle(
      `url('/assets/images/movie-scene-large/${this.backgroundImgName}.jpg')`
    );
  }

}
