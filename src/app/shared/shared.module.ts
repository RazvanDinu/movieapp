import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SetBackgroundDirective} from './set-background.directive';
import {FooterComponent} from '../footer/footer.component';
import {PageNotFoundComponent} from '../page-not-found/page-not-found.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    SetBackgroundDirective,
    FooterComponent,
    PageNotFoundComponent,
  ],
  exports: [
    SetBackgroundDirective,
    FooterComponent,
  ]
})
export class SharedModule { }
