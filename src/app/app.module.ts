import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import { NgModule } from '@angular/core';
// Redux
import {NgReduxModule} from '@angular-redux/store';
// app components
import { AppComponent } from './app.component';
// app modules
import {AppRoutingModule} from './app-routing.module';
import {CoreModule} from './core/core.module';
import {HeaderModule} from './header/header.module';
import {MoviesModule} from './movies/movies.module';
import {SharedModule} from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgReduxModule,
    // app modules
    CoreModule,
    SharedModule,
    HeaderModule,
    MoviesModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
