import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import 'rxjs/add/operator/map';

import {MoviesService} from '../core/movies.service';
import {Movie} from '../movies/shared/movie';
import {genreType, GenreType} from '../movies/shared/genre-type';
import {Filter} from '../movies/shared/filters';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {

  movies: Movie[];
  listClassType: string;
  genreTypes: GenreType[];
  filters: Filter;
  moviesYears: number[];
  currentPage: number;

  constructor(
    protected moviesService: MoviesService,
    protected router: Router,
    protected route: ActivatedRoute
  ) {
    this.listClassType = 'list-group-item';
    this.moviesYears = [];
    this.movies = [];
    this.genreTypes = [
      genreType.action,
      genreType.adventure,
      genreType.biography,
      genreType.comedy,
      genreType.crime,
      genreType.drama,
      genreType.history,
      genreType.mystery,
      genreType.thriller,
      genreType.scifi,
      genreType.sport
    ];
    this.filters = {
      searchTerm: '',
      genre: ''
    };
    this.currentPage = 1;
  }

  ngOnInit() {
    this.route
      .paramMap
      .map(paramsMap => paramsMap).subscribe(paramsMap => {
        this.filters.searchTerm = paramsMap.get('search') || '';
        this.filters.genre = paramsMap.get('genre') || '';
        this.currentPage = 1;
        this.getMovies();
    });
  }

  getMovies() {
    console.log(this.filters);
    this.moviesService.findMovies(this.currentPage, this.filters)
        .retry(2)
        .subscribe(
          data => {
          if (data.length) {
            if (this.currentPage === 1 && data.length <= 12) { // prevent doubling on search
              this.movies = data;
            } else {
              this.movies = [...this.movies, ...data];
            }
          } else {
            if (this.currentPage === 1) { // prevent removing all movie on last empty page
              this.movies = [];
            }
            this.currentPage = 0;
          }
        },
        (error: HttpErrorResponse) => {
          //  TODO: maybe a logger service?
          console.error(error.message);
        }
      );
  }

  viewMovieDetails(slug) {
    this.router.navigate(['movies', slug]);
  }

  changeListView(viewName) {
    this.listClassType = viewName;
  }

  updateFilters() {
    this.router.navigate(['movies', this.filters]);
  }

  loadMore() {
    if (this.currentPage > 0) {
      this.currentPage++;
      this.getMovies();
    }
  }

}
