import {Component, ElementRef, Input, OnInit, Renderer2, ViewChild} from '@angular/core';

import {Movie} from '../movies/shared/movie';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  @Input() movies: Movie[];
  currentIndex: number;
  @ViewChild('slide') slideView: ElementRef;
  @ViewChild('slideContainer') containerView: ElementRef;
  @ViewChild('nextButton') nextButtonView: ElementRef;
  @ViewChild('prevButton') prevButtonView: ElementRef;
  carouselSlides: ElementRef[];
  maxSlides: number;

  constructor(protected renderer: Renderer2) {
    this.currentIndex = 0;
  }

  ngOnInit() {}

  initCarousel() {
    console.log('init container width');
    const slideWidth = this.getSlideWidth();
    if (slideWidth < 10 || !this.containerView) {
      return;
    }
    this.maxSlides = this.movies ? this.movies.length : 0;
    const containerWidth = slideWidth * this.maxSlides;
    this.renderer.setAttribute(this.containerView.nativeElement, 'style', 'width: ' + containerWidth + 'px');
    this.carouselSlides = this.containerView.nativeElement.children;
    for (let i = 0; i <= this.maxSlides - 1; i++) {
      const child = this.containerView.nativeElement.children[i];
      this.renderer.setAttribute(child, 'style', 'width: ' + slideWidth + 'px');
    }
  }

  next() {
    console.log('next slide ' + this.currentIndex);
    if (this.currentIndex === this.maxSlides - 2) {
      this.renderer.setAttribute(this.nextButtonView.nativeElement, 'class', 'right carousel-control disabled');
    }
    if (this.currentIndex < this.maxSlides - 1) {
      this.moveSlide('right');
    }
  }

  prev() {
    console.log('prev slide ' + this.currentIndex);
    if (this.currentIndex === 1) {
      this.renderer.setAttribute(this.prevButtonView.nativeElement, 'class', 'left carousel-control disabled');
    }
    if (this.currentIndex > 0) {
      this.moveSlide('left');
    }
  }

  slideTo(i) {
    console.log(i);
    this.currentIndex = i;
  }

  moveSlide(direction) {
    this.renderer.setAttribute(this.carouselSlides[this.currentIndex], 'class', 'item');
    if (direction === 'right') {
      this.currentIndex++;
      this.renderer.setAttribute(this.prevButtonView.nativeElement, 'class', 'left carousel-control');
    } else {
      this.currentIndex--;
      this.renderer.setAttribute(this.nextButtonView.nativeElement, 'class', 'right carousel-control');
    }
    this.renderer.setAttribute(this.carouselSlides[this.currentIndex], 'class', 'item active');
  }

  protected getSlideWidth() {
    if (!this.slideView) {
      return;
    }

    return this.slideView.nativeElement.offsetWidth;
  }

}
