import {Component, Input, OnInit} from '@angular/core';
import {GenreType} from '../movies/shared/genre-type';
import {Filter} from '../movies/shared/filters';

@Component({
  selector: 'app-movie-genres',
  templateUrl: './movie-genres.component.html',
  styleUrls: ['./movie-genres.component.scss']
})
export class MovieGenresComponent implements OnInit {

  @Input() genres: GenreType[];
  @Input() filters: Filter;

  constructor() { }

  ngOnInit() {
  }

}
