import { Component, OnInit } from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import 'rxjs/add/operator/retry';

import {MoviesService} from '../core/movies.service';
import {Movie} from './shared/movie';
import {MovieDetails} from './shared/movie-details';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss'],
  providers: [
    MoviesService
  ]
})
export class MoviesComponent implements OnInit {

  newestMovies: Movie[];
  awardedMovies: MovieDetails[];

  constructor(protected moviesService: MoviesService) {
    this.awardedMovies = [];
  }

  ngOnInit() {
    this.moviesService.getNewestMovies()
      .retry(2)
      .subscribe(
        data => this.newestMovies = data,
        (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      );
    this.moviesService.getAwardedMovies()
      .retry(2)
      .subscribe(
        data => this.awardedMovies = data,
        (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      );
  }

}
