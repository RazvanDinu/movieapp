import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import { MoviesComponent } from './movies.component';
import {CarouselComponent} from '../carousel/carousel.component';
import {MovieDetailsComponent} from '../movie-details/movie-details.component';
import {MoviesListComponent} from '../movies-list/movies-list.component';
import {MoviesRoutingModule} from './movies-routing.module';
import {MovieGenresComponent} from '../movie-genres/movie-genres.component';
import {MoviesNewestComponent} from '../movies-newest/movies-newest.component';
import {MoviesAwardedComponent} from '../movies-awarded/movies-awarded.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MoviesRoutingModule,
    SharedModule,
  ],
  declarations: [
    MoviesComponent,
    CarouselComponent,
    MovieDetailsComponent,
    MoviesListComponent,
    MovieGenresComponent,
    MoviesNewestComponent,
    MoviesAwardedComponent,
  ]
})
export class MoviesModule {}
