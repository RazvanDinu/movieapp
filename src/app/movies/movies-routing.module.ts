import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {MoviesListComponent} from '../movies-list/movies-list.component';
import {MovieDetailsComponent} from '../movie-details/movie-details.component';

const movieRoutes: Routes = [
  { path: 'movies', children: [
    {
      path: '',
      component: MoviesListComponent
    },
    {
      path: ':slug',
      component: MovieDetailsComponent
    }
  ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(movieRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class MoviesRoutingModule {}
