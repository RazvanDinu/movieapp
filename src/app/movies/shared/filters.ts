import {GenreType} from './genre-type';

export interface Filter {
  searchTerm: string;
  genre: GenreType|string;
}
