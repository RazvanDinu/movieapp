import {ActivatedRoute, Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {Location} from '@angular/common';
import 'rxjs/add/operator/retry';

import {MovieDetails, Details} from '../movies/shared/movie-details';
import {MoviesService} from '../core/movies.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {

  movieDetails: Details;
  slug: string;

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected movieService: MoviesService,
    protected location: Location
  ) { }

  ngOnInit() {
    this.slug = this.route.snapshot.params['slug'];
    this.movieService.getMovieDetails(this.slug)
      .retry(2)
      .subscribe(
        data => this.processResponse(data),
        (error: HttpErrorResponse) => {
          console.error(error.message);
        }
      );
  }

  protected processResponse(data) {
    if (data[0]) {
      this.movieDetails = data[0].details;
    } else {
      this.router.navigateByUrl('/page-not-found');
    }
  }

  goBack() {
    this.location.back();
  }

}
