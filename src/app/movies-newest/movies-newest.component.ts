import {Component, Input, OnInit} from '@angular/core';

import {Movie} from '../movies/shared/movie';
import {Router} from '@angular/router';

@Component({
  selector: 'app-movies-newest',
  templateUrl: './movies-newest.component.html',
  styleUrls: ['./movies-newest.component.scss']
})
export class MoviesNewestComponent implements OnInit {

  @Input() movies: Movie[];

  constructor(
    protected router: Router
  ) {

  }

  ngOnInit() { }

  showDetail(slug) {
    console.log(slug);
    this.router.navigate(['movies', slug]); // TODO: maybe use promise to add/remove spinner?
  }

}
