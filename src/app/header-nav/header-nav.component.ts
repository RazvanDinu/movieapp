import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.scss']
})
export class HeaderNavComponent implements OnInit {

  searchTerm: string;

  constructor(protected router: Router) { }

  ngOnInit() {
  }

  search() {
    if (!this.searchTerm) {
      this.searchTerm = '';
    }

    this.router.navigate(['movies', {'search': this.searchTerm}]);
  }

}
