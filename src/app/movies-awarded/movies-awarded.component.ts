import {Component, Input, OnInit} from '@angular/core';
import {MovieDetails} from '../movies/shared/movie-details';

@Component({
  selector: 'app-movies-awarded',
  templateUrl: './movies-awarded.component.html',
  styleUrls: ['./movies-awarded.component.scss']
})
export class MoviesAwardedComponent implements OnInit {

  @Input() movies: MovieDetails[];

  constructor() { }

  ngOnInit() {
  }

}
