import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Movie} from '../movies/shared/movie';
import {MovieDetails} from '../movies/shared/movie-details';

@Injectable()
export class MoviesService {

  constructor(private http: HttpClient) { }

  getNewestMovies() {
    return this.http.get<Movie[]>('/api/movies?page=1&_sort=year&_order=desc&_limit=5');
  }

  getMovieDetails(movieSlug) {
    return this.http.get<MovieDetails>('/api/movie-details/?slug=' + movieSlug);
  }

  findMovies(page, filters) {
    let queryUrl = '/api/movies?';
    if (filters.searchTerm) {
      queryUrl += 'q=' + filters.searchTerm + '&';
    }
    if (filters.genre) {
      if (!filters.searchTerm) {
        queryUrl += '&';
      }
      queryUrl += 'genres_like=' + filters.genre + '&';
    }
    queryUrl += '_page=' + page + '&_limit=12';

    return this.http.get<Movie[]>(queryUrl);
  }

  getAwardedMovies() {
    return this.http.get<MovieDetails[]>('/api/movie-details?details.Awards_like=Golden&_limit=3');
  }

}
